export interface Square {
    id: string;
    value: players
}

// export type possibleValues = 'X' | 'O' | null;

export type players = 'X' | 'O';

export interface Move {
    squareId: string;
    player: players;
    board: Square[];
}