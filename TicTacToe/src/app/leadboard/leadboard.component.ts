import { Component, OnInit } from '@angular/core';
import { Observable, of} from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'leadboard',
  templateUrl: './leadboard.component.html',
  styleUrls: ['./leadboard.component.scss']
})
export class LeadboardComponent implements OnInit {

  leadboard$: Observable<string[]>;

  constructor() { }

  ngOnInit(): void {
    this.leadboard$= of([]);
  }

  addResult(newResult: string) {
    this.leadboard$.pipe(
      map((results) => {
        results.push(newResult);
      }),
      shareReplay(1)
    ).toPromise();
  }

}
