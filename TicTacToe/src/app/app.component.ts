import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { LeadboardComponent } from './leadboard/leadboard.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TicTacToe';

  @ViewChild('leadboard') leadboard:LeadboardComponent;
  
  sendResult(result: string) {
    this.leadboard.addResult(result)
  }
}
