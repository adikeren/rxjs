import { Component, OnInit, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { Square, players, Move, } from '../../Types'
import { Observable, BehaviorSubject, Subject, of } from 'rxjs';
import { map, tap} from 'rxjs/operators'
import { SquareComponent } from '../square/square.component';

@Component({
  selector: 'game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent implements OnInit {

  @Output() gameResult = new EventEmitter<string>();
  @ViewChildren(SquareComponent) squares: QueryList<SquareComponent>
  squares$: Observable<Square[]>;
  currentPlayer$: Observable<players>;
  playerSubject$: BehaviorSubject<players>;
  gameStatus$: Observable<string>;
  statusSubject$: BehaviorSubject<string>;
  isGameOver: boolean = false;
  makeMove$: Subject<Move> = new Subject();

  constructor() { }

  ngOnInit(): void {
    this.initGame();

    const markSquare$ = this.makeMove$.pipe(
      tap(move => {
        move.board.find(square => {return square.id === move.squareId}).value = move.player;
      })
    )
    
    const calculateResults$ = markSquare$.pipe(
      map((move) => {
        let newStatus: string;
        if(this.checkWin(move)) {
          newStatus = move.player + " won!";
          this.endGame(newStatus);
        } else {
          if(this.checkDraw(move.board)) {
            newStatus = "Draw!";
            this.endGame(newStatus);
          } else {
            if(move.player === 'X') {
              this.switchPlayer('O');
            } else {
              this.switchPlayer('X');
            }
            newStatus = "It's " + this.playerSubject$.value + " turn!";
          }
        }
        return newStatus;
      })
    )

    const updateStatus$ = calculateResults$.pipe(
      map((status) => this.setGameStatus(status))
    )

    updateStatus$.subscribe();
  }

  onClicked(squareId: string, player: players, board: Square[]) {
    this.makeMove$.next({squareId: squareId, player: player, board: board})
  }

  initGame() {
    this.initBoard();
    this.initPlayer();
    this.initGameStatus();
    this.isGameOver = false;
   }

  initBoard() {
    this.squares$ = of([
      { id: '1', value: null },
      { id: '2', value: null },
      { id: '3', value: null },
      { id: '4', value: null },
      { id: '5', value: null },
      { id: '6', value: null },
      { id: '7', value: null },
      { id: '8', value: null },
      { id: '9', value: null },
    ]);
  }

  initPlayer() {
    this.playerSubject$ = new BehaviorSubject('X');
    this.currentPlayer$ = this.playerSubject$.asObservable();
  }

  initGameStatus() {
    this.statusSubject$ = new BehaviorSubject("It's " + this.playerSubject$.getValue() + " turn!");
    this.gameStatus$ = this.statusSubject$.asObservable();
  }

 setGameStatus(newStatus: string) {
  this.statusSubject$.next(newStatus);
 }

 checkWin(move: Move): boolean {
   const possibleWins: string[][] = [
      ['1', '2', '3'],
      ['4', '5', '6'],
      ['7', '8', '9'],
      ['1', '4', '7'],
      ['2', '5', '8'],
      ['3', '6', '9'],
      ['1', '5', '9'],
      ['3', '5', '7']
   ];

   let isWin: boolean = false;
  for (let i = 0; i < possibleWins.length && !isWin; i++) {
      const squareOneValue: players = this.getSquareValue(move.board, possibleWins[i][0]);
      const squareTwoValue: players = this.getSquareValue(move.board, possibleWins[i][1]);
      const squareThreeValue: players = this.getSquareValue(move.board, possibleWins[i][2]);

      if(squareOneValue === squareTwoValue && squareTwoValue === squareThreeValue &&
          squareOneValue != null && squareTwoValue != null) {
        isWin = true;
      }
  }

  return isWin;
 }

 getSquareValue(board: Square[], squareId: string): players {
   return board.find((square) => {return square.id === squareId}).value;
 }

  checkDraw(board: Square[]): boolean {
    let isDraw: boolean = true;
        for(let i = 0; i < board.length && isDraw === true; i++) {
          if (board[i].value === null) {
            isDraw = false;
          }
        }

    return isDraw;
  }

  switchPlayer(nextPlayer: players) {
    this.playerSubject$.next(nextPlayer);
  }

  endGame(result: string) {
  this.isGameOver = true;
  this.gameResult.emit(result);
  this.squares.forEach((square) => square.isClicked = true);
  }
}
  

