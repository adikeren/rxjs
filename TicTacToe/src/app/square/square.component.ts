import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Square } from 'src/Types';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {

  @Input() square: Square;
  @Output() clicked = new EventEmitter<string>();
  isClicked: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  clickSquare() {
    this.isClicked = true;
    this.clicked.emit(this.square.id);
  }

}
